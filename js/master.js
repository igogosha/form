/* datepicker */
$('#articleDate').datepicker({
    format: 'yyyy-mm-dd'
});

/* rich text editor */
 $('#articleFullText').rte({
        width: 600,
        height: 300,
        controls_rte: rte_toolbar
});

/* form client-side validation */
$('#articleSave').on('click', function(event){
    event.preventDefault();
    var date = $('#articleDate').val();
    var name = $('#articleName').val();
    var full = $('iframe#articleFullText').contents().find('body').text();
    if ( date && name && full && date.match(/^[0-9]{4}-|\s[0-9]{2}-|\s[0-9]{2}$/) ) {
        $('#articleForm').submit();
    } else {
        $('#error').slideDown().delay(3000).slideUp();
    }
});


