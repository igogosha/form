<!DOCTYPE html>
<html>
    <head>
        <title>Форма добавления статьи</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        
        <!-- rich text editor for article -->
        <link rel="stylesheet" type="text/css" href="plugins/rte/jquery.rte.css" />
        
        <link rel="stylesheet" type="text/css" href="css/master.css" />
    </head>
    <body>
        
        <div class="container articleForm">
            
        <div class="pull-right">
            <a href="?action=add" type="button" class="btn btn-default">Добавить статью</a>
            <a href="?action=show" type="button" class="btn btn-default">Список статей</a>
        </div>
            
        <div class="clear clearfix"></div>