<?php if ( isset($_GET['success']) && $_GET['success'] == '1' ) { ?>
    <div class="alert alert-success fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
        <strong>УРА!</strong> Вы успешно добавили статью!
    </div>
<?php } ?>

<?php if ( isset($_GET['error']) && $_GET['error'] == '1' ) { ?>
    <div class="alert alert-danger fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
        <strong>УПС!</strong> Что-то пошло не так!
    </div>
<?php } ?>

<fieldset>
    <legend>Добавление статьи</legend>
    <form class="form-horizontal" id="articleForm" action="/form/save.php" method="post" enctype="multipart/form-data">

    <div class="control-group">
        <label class="control-label" for="articleDate">Дата<sup>*</sup></label>
      <div class="controls">
          <input id="articleDate" value="<?php echo date('Y-m-d') ?>" name="articleDate" type="text" placeholder="Дата добавления статьи" class="form-control">

      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="articleName">Название<sup>*</sup></label>
      <div class="controls">
        <input id="articleName" maxlength="200" name="articleName" type="text" placeholder="Название статьи" class="form-control">

      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="articleFullText">Основной текст<sup>*</sup></label>
      <div class="controls">                     
        <textarea id="articleFullText" class="articleFullText" name="articleFullText"></textarea>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="articleTags">Тэги</label>
      <div class="controls">
        <input id="articleTags" name="articleTags" type="text" placeholder="тэги через запятую" class=" form-control">

      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="articleImage">Картинка</label>
      <div class="controls">
        <input id="articleImage" name="articleImage" class="input-file" type="file">
      </div>
    </div>

    <div class="control-group" id="error">
      <div class="controls">
          <label class="control-label"></label>
          <div class="alert alert-danger">Заполните все обязательные поля!</div>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="articleSave"></label>
      <div class="controls">
        <button id="articleSave" name="articleSave" class="btn btn-danger  form-control">Сохранить</button>
      </div>
    </div>
    </form>
</fieldset>
        
