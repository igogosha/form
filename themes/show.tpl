<?php
include_once('database.php');
$db = new Database();
$articles = $db->select("SELECT * FROM article");

if ( empty($articles) ) { ?>
    <div class="well well-lg">
        <fieldset>
            <legend>Список статей пуст</legend>
        </fieldset>
    </div>
<?php } else {

    foreach( $articles as $article ) {
    ?>

    <div class="articleItem">
        <fieldset>
            <legend><?php echo $article->articleName ?></legend>

            <div class="label label-warning pull-right"><?php echo $article->articleDate ?></div>
            <img src="http://localhost/form/uploads/<?php echo $article->articleImage ?>" class="img img-responsive" />
            <p class=""><?php echo $article->articleFull ?></p>
            
            Тэги:
            <?php  $tags = explode(',',$article->articleTags ); ?>
            <?php foreach ( $tags as $tag ) { ?>
                <a href="" class="bn btn-xs btn-warning"><?php echo $tag ?></a>
            <?php } ?>
        </fieldset>
    </div>

    <?php } ?>
 <?php } ?>

