<?php

/* если сохранили статью, добавляем ее в БД и копируем файл картинки на сервер */
if ( isset($_POST['articleName']) ) {
    include_once('database.php');
    $db = new Database();
    
    if ( !$_POST['articleDate'] || !$_POST['articleName'] || !$_POST['articleFullText'] ) {
        header("Location: http://localhost/form/");
    }

    $articleDate = mysql_real_escape_string($_POST['articleDate']);
    $articleName = mysql_real_escape_string($_POST['articleName']);
    $articleFull = mysql_real_escape_string($_POST['articleFullText']);

    $postdata = array(
        'articleDate' => $articleDate,
        'articleName'  => $articleName,
        'articleFull'     => $articleFull,
    );
    if ( $_POST['articleTags'] ) {
        $postdata['articleTags'] = mysql_real_escape_string($_POST['articleTags']);
    }
    if ( $_FILES['articleImage'] ) {
        if ( move_uploaded_file($_FILES["articleImage"]["tmp_name"], "uploads/" . $_FILES["articleImage"]["name"]) ) {
            $postdata['articleImage'] = mysql_real_escape_string($_FILES['articleImage']['name']);
        }
    }
    $db->insert('article', $postdata);
    header("Location: http://localhost/form/?action=add");
}